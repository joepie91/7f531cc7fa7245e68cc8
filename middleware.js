module.exports = function databaseMiddleware(path) {
	database = sqlite.open(path);
	
	return function(req, res, next) {
		req.database = database;
		next();
	}
}